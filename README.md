<img src=".gitlab/logo_thumb_small.png" alt="Logo" align="left" width="85" height="85"/>

### Plata-theme-flatpak

Plata-theme sources for Flatpak packaging only.

-----------------------------------------------

<img src=".gitlab/warning.png" alt="Logo" align="left" width="85" height="85"/>

### 05 Jan, 2019

Branch `3.24` has been unusable, we switched to `3.22` new master branch.

Specify the branch like `flatpak install flathub PACKAGE_NAME//BRANCH` when installing.

-----------------------------------------------

Public License
--------------

 * GPLv2.0 (Codebase)

 * CC BY-SA 4.0 (Asset images)
